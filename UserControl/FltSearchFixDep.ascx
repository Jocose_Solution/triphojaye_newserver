﻿<%@ Control Language="VB" AutoEventWireup="true" CodeFile="FltSearchFixDep.ascx.vb" Inherits="UserControl_FltSearchFixDep" %>

<%@ Register Src="~/UserControl/HotelSearch.ascx" TagPrefix="uc1" TagName="HotelSearch" %>
<%@ Register Src="~/UserControl/HotelDashboard.ascx" TagPrefix="uc1" TagName="HotelDashboard" %>

<link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
    rel="stylesheet" />

<style type="text/css">
    /*.dropdown-container {
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 40px 0 0 0;
}*/

    .dropdown {
        /*background: #f8f8f8;*/
        padding: 20px;
        /*border-radius: 3px;
  width: 140px;*/
        display: flex;
        justify-content: space-around;
        font-size: 1.1rem;
        cursor: pointer;
        /*box-shadow: 0 0 1px rgba(0, 0, 0, 0.3);*/
    }

    .fa-angle-down {
        position: relative;
        top: 2px;
        font-size: 1.3rem;
        transition: transform 0.3s ease;
    }

    .rotate-dropdown-arrow {
        transform: rotate(-180deg);
    }

    .dropdown-menu {
        display: none;
        flex-direction: column;
        border-radius: 4px;
        margin-top: 8px;
        width: 160px;
        padding: 10px;
        box-shadow: 0 0 5px -1px rgba(0, 0, 0, 0.3);
        background: #fafafa;
        transform-origin: top left;
    }

        .dropdown-menu span {
            padding: 10px;
            flex-grow: 1;
            width: 100%;
            box-sizing: border-box;
            text-align: center;
            cursor: pointer;
            transition: background 0.3s ease;
        }

            .dropdown-menu span:last-child {
                border: none;
            }

            .dropdown-menu span:hover {
                background: #eee;
            }

    #openDropdown:checked + .dropdown-menu {
        display: flex;
        animation: openDropDown 0.4s ease;
    }

    @keyframes openDropDown {
        from {
            transform: rotateX(50deg);
        }

        to {
            transform: rotateX(0deg);
        }
    }
</style>



<style type="text/css">
    .minus, .plus {
        width: 35px;
        height: 35px;
        background: #404040;
        border-radius: 50%;
        padding: 8px 5px 8px 5px;
        /* border: 1px solid #ddd; */
        display: inline-block;
        vertical-align: middle;
        text-align: center;
        cursor: pointer;
        color: #fff;
    }

    .inp {
        width: 30px;
        text-align: center;
        color: #000;
        background: none;
        border: none;
    }

    .main_dv {
        width: 100%;
        float: left;
        margin-bottom: 13px;
    }

    .ttl_col {
        width: 35%;
        float: left;
    }

        .ttl_col span {
            font-size: 10px;
            color: #a3a2a2;
            display: block;
        }

        .ttl_col p {
            font-size: 13px;
            color: #000;
            display: block;
        }

    .dn_btn {
        cursor: pointer;
        background: #ff0000;
        float: right;
        text-align: center;
        padding: 4px 12px;
        display: block;
        color: #fff;
        font-size: 11px;
        border-radius: 3px;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
    }

    .innr_pnl {
        width: 200px;
        position: relative;
        /*padding: 10px;*/
    }

    .dropdown-content-n2 {
        /*display:none;*/
        position: absolute;
        background-color: #fff;
        width: 200px;
        padding: 10px;
        box-shadow: 0 0 20px 0 rgba(0,0,0,0.45);
        z-index: 1;
        /*top: 114px;*/
        box-sizing: content-box;
        -webkit-box-sizing: content-box;
        right: 349px
    }

    .innr_pnl::before {
        content: '';
        position: absolute;
        left: 2%;
        top: -15px;
        width: 0;
        height: 0;
        border-left: 5px solid transparent;
        border-right: 5px solid transparent;
        border-bottom: 5px solid #fff;
        clear: both;
    }

    .clear {
        clear: both;
    }
</style>



<div>
    <div class="topwaysF" style="display: none">

        <div class="col-md-1 col-xs-4 nopad text-search">
            <label class="mail active">
                <input type="radio" style="display: none;" name="TripTypeF" value="rdbOneWayF" id="rdbOneWayF" checked="checked" />
                One Way</label>
        </div>
        <div class="col-md-2 col-xs-4 nopad text-search">
            <label class="mail">
                <input type="radio" name="TripTypeF" style="display: none;" value="rdbRoundTripF" id="rdbRoundTripF" />
                Round Trips</label>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var selector = '.topwaysF div label'; //mk
            $(selector).bind('click', function () {
                $(selector).removeClass('active');
                $(this).addClass('active');
            });

        });
    </script>
    <%--<div class="col-md-4 col-xs-6 text-search">

        <input id="CB_GroupSearch" type="checkbox" value="grpSearch" name="GroupSearch" />
        Group Search       
    </div>--%>
</div>


<div class="tab-content _pt-20 fxd" style="padding: 17px;margin-top: -37px;display:none;">

       <div class="tab-pane active" id="SearchAreaTabs-3" role="tab-panel">
        <div class="theme-search-area theme-search-area-stacked">
            <div class="theme-search-area-form">
                <div class="row" data-gutter="none">

            <div class="col-md-6">

                <div class="row" data-gutter="none">
                    <div class="col-md-12 ">
                        <div class="theme-search-area-section first theme-search-area-section-curved theme-search-area-section-bg-white theme-search-area-section-no-border theme-search-area-section-mr">
                            <div class="theme-search-area-section-inner">
                                <i class="theme-search-area-section-icon lin lin-location-pin"></i>


                                <asp:DropDownList ID="Sector" runat="server" class="theme-search-area-section-input typeahead"></asp:DropDownList>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" style="display: none">
                        <label for="exampleInputEmail1">
                            Search Sector:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtDepCityFD" class="form-control" placeholder="Enter Your Departure City" value="" onclick="this.value = '';" id="txtDepCityFD" />
                            <input type="hidden" id="hidtxtDepCityFD" name="hidtxtDepCityFD" value="TT" />
                        </div>
                    </div>

                    <div class="form-group" style="display: none">
                        <label for="exampleInputEmail1">
                            Leaving From:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtDepCity1F" class="form-control" placeholder="Enter Your Departure City" value="New Delhi, India-Indira Gandhi Intl(DEL)" onclick="this.value = '';" id="txtDepCity1F" />
                            <input type="hidden" id="hidtxtDepCity1F" name="hidtxtDepCity1F" value="ZZZ,IN" />
                        </div>
                    </div>
                </div>

            </div>


            <div class="col-md-6 ">
                <div class="row" data-gutter="none">

                    <div class="col-md-6 " id="TravellerF">
                        <div class="theme-search-area-section theme-search-area-section-curved theme-search-area-section-bg-white theme-search-area-section-no-border theme-search-area-section-mr quantity-selector" data-increment="Passengers">
                            <div class="theme-search-area-section-inner">
                                <i class="theme-search-area-section-icon lin lin-people"></i>
                                <input class="theme-search-area-section-input div" id="sapnTotPaxF" placeholder=" Traveller" type="text" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                <input type="checkbox" class="chkNonstop" name="chkNonstop" id="chkNonstop" value="Y" style="display: none" />
                <button type="button" id="btnSearchF" value="Search" class="theme-search-area-submit _mt-0 theme-search-area-submit-no-border theme-search-area-submit-curved ">Search</button>

            </div>

                </div>
            </div>

            

            </div>
                </div>
            </div>
        </div>








            <div class="onewayss col-md-5 nopad text-search mltcs" style="display: none">
                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Going To:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-map-marker fa-lg yellow"></i>
                        </div>
                        <input type="text" name="txtArrCity1F" onclick="this.value = '';" id="txtArrCity1F" value="New Delhi, India-Indira Gandhi Intl(DEL)" class="form-control" placeholder="Enter Your Destination City" />
                        <input type="hidden" id="hidtxtArrCity1F" name="hidtxtArrCity1F" value="ZZZ,IN" />
                    </div>
                </div>
            </div>
            <div class="col-md-2 nopad text-search mrgs" id="oneF" style="display: none">
                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Depart Date:</label>
                    <div class="input-group">

                        <div class="input-group-addon">
                            <i class="fa fa-calendar yellow"></i>
                        </div>
                        <input type="text" class="form-control" placeholder="dd/mm/yyyy" name="txtDepDateF" id="txtDepDateF" value=""
                            readonly="readonly" />
                        <input type="hidden" name="hidtxtDepDate" id="hidtxtDepDateF" value="" />

                    </div>
                </div>
            </div>
            <div class="col-md-2 nopad text-search mrgs" id="ReturnF" style="display: none">
                <div class="form-group" id="trRetDateRowF" style="display: none;">
                    <label for="exampleInputEmail1">
                        Return Date:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar yellow"></i>
                        </div>
                        <input type="text" placeholder="dd/mm/yyyy" name="txtRetDateF" id="txtRetDateF" class=" form-control" value=""
                            readonly="readonly" />
                        <input type="hidden" name="hidtxtRetDateF" id="hidtxtRetDateF" value="" />


                    </div>
                </div>
            </div>
            <div style="display: none;" id="twoF">
                <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity2F">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtDepCity2F" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity2F" />
                            <input type="hidden" id="hidtxtDepCity2F" name="hidtxtDepCity2F" value="" />
                        </div>
                    </div>
                </div>

                <div class="onewayss col-md-4 nopad text-search mltcs">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtArrCity2F" class="form-control" placeholder="Enter Your Destination City" id="txtArrCity2F" />
                            <input type="hidden" id="hidtxtArrCity2F" name="hidtxtArrCity2F" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 nopad text-search mrgs" id="DivArrCity2F">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar yellow"></i>
                            </div>
                            <input type="text" name="txtDepDate2" id="txtDepDate2F" class=" form-control" placeholder="dd/mm/yyyy" readonly="readonly" value="" />
                            <input type="hidden" name="hidtxtDepDate2F" id="hidtxtDepDate2F" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none;" id="threeF">

                <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity3F">
                    <div class="form-group">

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtDepCity3F" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity3F" />
                            <input type="hidden" id="hidtxtDepCity3F" name="hidtxtDepCity3F" value="" />
                        </div>
                    </div>
                </div>
                <div class="onewayss col-md-4 nopad text-search mltcs">
                    <div class="form-group">

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtArrCity3F" class="form-control" placeholder="Enter Your Destination City" id="txtArrCity3F" />
                            <input type="hidden" id="hidtxtArrCity3F" name="hidtxtArrCity3F" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 nopad text-search mrgs" id="DivArrCity3F">
                    <div class="form-group">

                        <div class="input-group">

                            <div class="input-group-addon">
                                <i class="fa fa-calendar yellow"></i>
                            </div>
                            <input type="text" name="txtDepDate3F" id="txtDepDate3F" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate3F" id="hidtxtDepDate3F" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none;" id="four">
                <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity4F">
                    <div class="form-group">

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtDepCity4F" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity4F" />
                            <input type="hidden" id="hidtxtDepCity4F" name="hidtxtDepCity4F" value="" />
                        </div>
                    </div>
                </div>
                <div class="onewayss col-md-4 nopad text-search mltcs">
                    <div class="form-group">

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtArrCity4F" class="form-control" placeholder="Enter Your Destination City" id="txtArrCity4F" />
                            <input type="hidden" id="hidtxtArrCity4F" name="hidtxtArrCity4F" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 nopad text-search mrgs" id="DivArrCity4F">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar yellow"></i>
                            </div>
                            <input type="text" name="txtDepDate4F" id="txtDepDate4F" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate4F" id="hidtxtDepDate4F" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none;" id="five">
                <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity5">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtDepCity5F" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity5F" />
                            <input type="hidden" id="hidtxtDepCity5F" name="hidtxtDepCity5F" value="" />
                        </div>
                    </div>
                </div>
                <div class="onewayss col-md-4 nopad text-search mltcs">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtArrCity5F" class="form-control" placeholder="Enter Your Destination City" onclick="this.value = '';" id="txtArrCity5F" />
                            <input type="hidden" id="hidtxtArrCity5F" name="hidtxtArrCity5F" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 nopad text-search mrgs" id="DivArrCity5F">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar yellow"></i>
                            </div>
                            <input type="text" name="txtDepDate5" id="txtDepDate5F" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate5F" id="hidtxtDepDate5F" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none;" id="six">
                <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity6F">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtDepCity6F" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity6F" />
                            <input type="hidden" id="hidtxtDepCity6F" name="hidtxtDepCity6" value="" />
                        </div>
                    </div>
                </div>

                <div class="onewayss col-md-4 nopad text-search mltcs">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtArrCity6F" class="form-control" placeholder="Enter Your Destination City" onclick="this.value = '';" id="txtArrCity6F" />
                            <input type="hidden" id="hidtxtArrCity6F" name="hidtxtArrCity6F" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 nopad text-search mrgs" id="ArrCity6F">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar yellow"></i>
                            </div>
                            <input type="text" name="txtDepDate6F" id="txtDepDate6F" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate6F" id="hidtxtDepDate6F" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="clear"></div>

            <div class="row col-md-5 col-xs-12 pull-right" id="addF" style="display: none">
                <div class="col-md-4 col-xs-4 text-search text-right">
                    <a id="plusF" class="pulse text-search">Add City</a>
                </div>
                <div class="col-md-4 col-xs-4 text-search  text-right">
                    <a id="minusF" class="pulse text-search">Remove City</a>
                </div>
            </div>
      

        <div class="row" style="display: none">
            <div class="text-search col-md-5 col-xs-12" style="padding-bottom: 10px; cursor: pointer;" id="advtravelF">Advanced options <i class="fa fa-chevron-right" aria-hidden="true"></i></div>
            <div class="col-md-12 advopt" id="advtravelssF" style="display: none; overflow: auto;">
                <div class="col-md-3 nopad text-search">
                    <div class="form-group">
                        <label for="exampleInputEmail1">
                            Airlines</label>
                        <input type="text" placeholder="Search By Airlines" class="form-control" name="txtAirline" value="" id="txtAirlineF" />
                        <input type="hidden" id="hidtxtAirlineF" name="hidtxtAirlineF" value="" />

                    </div>
                </div>

                <div class="col-md-3 col-xs-12 text-search">
                    <div class="form-group">
                        <label for="exampleInputEmail1">
                            Class Type</label>
                        <select name="CabinF" class="form-control" id="CabinF">
                            <option value="" selected="selected">--All--</option>
                            <option value="C">Business</option>
                            <option value="Y">Economy</option>
                            <option value="F">First</option>
                            <option value="W">Premium Economy</option>
                        </select>

                    </div>
                </div>
            </div>
        </div>

 

    <div id="boxF" style="display: none;">
    <div id="div_Adult_Child_Infant" class="dropdown-content-n2 myText">
        <div class="innr_pnl">
            <div class="main_dv">
                <div class="ttl_col">
                    <p>Adult</p>
                    <span>(12+ yrs)</span>
                </div>
                <div class="number">
                    <span class="minus">-</span>
                    <input type="text" class="inp" value="1" min="1" name="AdultF" id="AdultF" />
                    <span class="plus">+</span>
                </div>

            </div>
            <div class="main_dv">
                <div class="ttl_col">
                    <p>Children</p>
                    <span>(2+ 12 yrs)</span>
                </div>

                <div class="number">
                    <span class="minus">-</span>
                    <input type="text" class="inp" value="0" min="0" name="ChildF" id="ChildF" />
                    <span class="plus">+</span>
                </div>

            </div>
            <div class="main_div">

                <div class="ttl_col">
                    <p>Infant(s)</p>
                    <span>(below 2 yrs)</span>
                </div>

                <div class="number">
                    <span class="minus">-</span>
                    <input type="text" class="inp" value="0" min="0" name="InfantF" id="InfantF" />
                    <span class="plus Infant">+</span>
                </div>

            </div>

            <div class="clear"></div>

            <a href="#" onclick="plusF()" class="dn_btn" id="serachbtnF">Done</a>


        </div>
    </div>
</div>



    <div class="clear1"></div>
    <div class="col-md-3 col-xs-12 text-search" id="trAdvSearchRowF" style="display: none">
        <div class="lft ptop10">
            All Fare Classes
        </div>
        <div class="lft mright10">

            <input type="checkbox" name="chkAdvSearchF" id="chkAdvSearchF" value="True" />
        </div>
        <div class="large-4 medium-4 small-12 columns">
            Gds Round Trip Fares
                                
                                <span class="lft mright10">
                                    <input type="checkbox" name="GDS_RTFF" id="GDS_RTFF" value="True" />
                                </span>
        </div>

        <div class="large-4 medium-4 small-12 columns">
            Lcc Round Trip Fares
                                
                                <span class="lft mright10">
                                    <input type="checkbox" name="LCC_RTFF" id="LCC_RTFF" value="True" />
                                </span>
        </div>

    </div>
</div>
<script>
    $(document).ready(function () {
        $("#advtravelF").click(function () {
            $("#advtravelssF").slideToggle();
        });


        $("#TravellerF").click(function () {
            $("#boxF").slideToggle();
        });
        $("#serachbtnF").click(function () {
            $("#boxF").slideToggle();
        });

    });
</script>








<%--<div id="div_Adult_Child_Infant1">
    <div class="row" id="boxF1">
        <i class="" aria-hidden="true"></i>
        <div class="col-md-7 col-xs-12 text-searchs">
            <div class="form-group">
                <div class="col-md-5 col-xs-5 travelpad">
                    <label class="lblss" for="exampleInputEmail1">
                        Adult (12+) Yrs
                    </label>
                </div>
                <div class="col-md-7 col-xs-7 ">
                    <select class="form-control" name="AdultF" id="AdultF1">
                        <option value="1" selected="selected">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>
                </div>

            </div>
        </div>
        <div class="col-md-7 col-xs-12 text-searchs">
            <div class="form-group">
                <div class="col-md-5 col-xs-5 travelpad">
                    <label class="lblss" for="exampleInputEmail1">
                        Child(2-12) Yrs</label>
                </div>
                <div class="col-md-7 col-xs-7 ">
                    <select class="form-control" name="ChildF" id="ChildF1">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-7 col-xs-12 text-searchs">
            <div class="form-group">
                <div class="col-md-5 col-xs-5 travelpad">
                    <label class="lblss" for="exampleInputEmail1">
                        Infant(0-2) Yrs</label>
                </div>
                <div class="col-md-7 col-xs-7 ">
                    <select class="form-control" name="InfantF" id="InfantF1">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-xs-12">
            <button type="button" onclick="plusF()" class="btn btn-danger" id="serachbtnF1" style="margin-top: 5px;">
                Done</button>
        </div>

    </div>
</div>--%>






<script type="text/javascript">
    function plusF() {
        document.getElementById("sapnTotPaxF").value = (parseInt(document.getElementById("AdultF").value.split(' ')[0]) + parseInt(document.getElementById("ChildF").value.split(' ')[0]) + parseInt(document.getElementById("InfantF").value.split(' ')[0])).toString() + ' Traveller';
    }
    plusF();
</script>
<script type="text/javascript">
    var myDate = new Date();
    var currDate = (myDate.getDate()) + '/' + (myDate.getMonth() + 1) + '/' + myDate.getFullYear();
    document.getElementById("txtDepDateF").value = currDate;
    document.getElementById("hidtxtDepDateF").value = currDate;
    document.getElementById("txtRetDateF").value = currDate;
    document.getElementById("hidtxtRetDateF").value = currDate;
    var UrlBase = '<%=ResolveUrl("~/") %>';
</script>

<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search3F.js") %>"></script>

<script type="text/javascript">

    $(function () {
        $("#CB_GroupSearchF").click(function () {

            if ($(this).is(":checked")) {
                // $("#box").hide();
                $("#TravellerF").hide();
                $("#rdbRoundTripF").attr("checked", true);
                $("#rdbOneWayF").attr("checked", false);

            } else {
                // $("#box").show();
                $("#TravellerF").show();
            }
        });
    });
</script>
