﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc-dmt-direct-fund-transfer.ascx.cs" Inherits="DMT_Manager_User_Control_dmtdirect_uc_dmt_direct_fund_transfer" %>

<style>
    .krishna {
        display: block;
        width: 100%;
        height: calc(1.1em + .75rem + 2px);
        padding: .1rem .8rem;
        font-size: 1rem;
        font-weight: 400;
        /* line-height: 1.5; */
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
</style>

<button type="button" class="btn btn-info btn-lg hidden addbenificiarydetail" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#FormBeneficiary"></button>
<div class="modal fade" id="FormBeneficiary" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content" style="text-align: center; border-radius: 5px !important;">
            <div class="modal-body" style="padding-left: 20px!important;">
                <div class="container" id="BeneficiarySectionForm">
                    <div class="login-signup-page mx-auto">
                        <h3 class="font-weight-400 text-center">New Beneficiary Detail
                                <button type="button" id="btnbenformclose" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                        </h3>
                        <div class="bg-light shadow-md rounded p-4 mx-2">
                            <div class="form-group form-validation">
                                <input id="txtBenAccountNo" type="text" class="form-control" placeholder="Enter Bank Account Number" onkeypress="return isNumberValidationPrevent(event);" />
                            </div>
                            <div class="form-group form-validation">
                                <select id="ddlBindBankDrop" class="form-control">
                                    <option value="">Select Bank</option>
                                </select>
                                <span id="ProcessBindingBank" class="hidden" style="position: absolute; right: 83px; top: 160px; font-size: 25px; color: #ff434f;"><i class='fa fa-pulse fa-spinner'></i></span>
                                <p id="typeofifsc" class="text-warning text-right"></p>
                            </div>
                            <div class="form-group form-validation">
                                <input id="txtBenIFSCNo" type="text" class="form-control" placeholder="Enter IFSC Number" />
                            </div>
                            <div class="form-group form-validation text-right">
                                <input id="txtBenPerName" type="text" class="form-control" placeholder="Enter Beneficiary Name" />
                                <span id="CheckAccountDetailPayout" class="text-primary btn pull-right btn-sm" style="cursor: pointer; padding: 0px!important;" onclick="return GetBeneficiaryNamePayout();">Get Beneficiary Name</span>
                            </div>
                            <%-- <div class="form-group form-validation">
                                <input id="txtBenPerMobile" type="text" class="form-control" placeholder="Enter Beneficiary Mobile Number" maxlength="10" autocomplete="off" onkeypress="return isNumberValidationPrevent(event);" />
                            </div>--%>
                            <span id="btnBenRegistration" style="cursor: pointer;" class="btn btn-primary btn-block my-4" onclick="javascript: return SubmitBeneficiaryDetail();">Submit</span>
                            <p id="benerrormessage" class="text-danger"></p>
                            <h6 id="bensuccessmessage" class="text-success"></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<button type="button" class="btn btn-info btn-lg hidden updateaddresspopup" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#UpdateAddressPopup"></button>
<div class="modal fade" id="UpdateAddressPopup" role="dialog">
    <div class="modal-dialog modal-md" style="margin: 5% auto!important;">
        <div class="modal-content" style="text-align: center; border-radius: 5px !important;">
            <div class="modal-body" style="padding-left: 20px!important;">
                <div class="container">
                    <div class="login-signup-page mx-auto">
                        <h4 class="font-weight-400 text-center">Update Current / Local Address
                                <button type="button" id="UpdateAddressClose" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                        </h4>
                        <div class="bg-light shadow-md rounded p-4 mx-2">
                            <div class="form-group form-validation">
                                <textarea id="txtUpdateCurrLocalAddress" class="form-control upper_letter" placeholder="Current / Local Address"></textarea>
                            </div>
                            <span id="btnUpdateCurrLocalAddress" style="cursor: pointer;" class="btn btn-primary btn-block my-4" onclick="return UpdateCurrLocalAddress();">Update Address</span>
                            <p id="updatemsg"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<button type="button" class="btn btn-primary hidden VerifyTransferDetails" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#VerifyTransferDetails">rtuy</button>
<div class="modal fade" id="VerifyTransferDetails">
    <div class="modal-dialog modal-lg" id="VerifyTransferModalDialog" style="margin: 2% auto!important;">
        <div class="modal-content" id="VerifyTransSummaryDetail" style="max-height: 630px;">
            <div class="modal-header">
                <h5 class="modal-title" id="transveryheading">Transaction Summary</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #ff414d;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="FundTransDetailBody"></div>
            <div class="modal-footer" id="FundTransDetailFooter"></div>
            <div class="col-sm-12 text-center" style="top: -60px;">
                <p id="paratranserror"></p>
            </div>
        </div>
    </div>
</div>

<button type="button" class="btn btn-info btn-lg hidden benificierypopupclass" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#BenificieryDetail"></button>
<div class="modal fade" id="BenificieryDetail" role="dialog">
    <div class="modal-dialog modal-md" style="margin: 10% auto!important;">
        <div class="modal-content" style="border-radius: 5px !important;">
            <div class="modal-header" style="padding: 12px;">
                <h5 class="modal-title popupheading text-success"><i class="fa fa-file" aria-hidden="true"></i>&nbsp;Benificiery Detail</h5>
            </div>
            <div class="modal-body" style="padding-left: 20px!important;" id="BenBodyContent"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" style="padding: 0.5rem 1rem!important;" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<button type="button" class="btn btn-info btn-lg hidden delbendetail" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#DeleteBenDetail"></button>
<div class="modal fade" id="DeleteBenDetail" role="dialog">
    <div class="modal-dialog modal-md" style="margin: 10% auto!important;">
        <div id="ConfirmBenDelete" class="modal-content" style="text-align: center; border-radius: 5px !important;">
            <div class="modal-body" style="padding: 30px!important;">
                <h6>Are you sure want to delete this benificiary detail?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #ff414d; margin: -50px -15px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer">
                <input id="hdnDeleteBenId" type="hidden" />
                <button type="button" id="btnDelConfirmed" class="btn btn-sm btn-success" style="padding: 0.5rem 1rem!important;" onclick="YesConfirmDeleteDen();">YES</button>
                <button type="button" class="btn btn-sm btn-danger" style="padding: 0.5rem 1rem!important;" data-dismiss="modal">NO</button>
            </div>
        </div>
        <div id="OTPConfirmBenDelete" class="modal-content hidden" style="text-align: center; left: 25%; width: 300px; border-radius: 5px !important;">
            <div class="modal-body form-validation" style="padding: 30px!important;">
                <h6>Please enter OTP</h6>
                <input id="txtBenDeleteOTP" type="text" class="form-control" />
            </div>
            <div class="modal-footer">
                <button type="button" id="btnOTPDelConfirmed" class="btn btn-sm btn-success" style="padding: 0.5rem 1rem!important;" onclick="ConfirmDeleteDen();">DELETE</button>
                <button type="button" class="btn btn-sm btn-danger" style="padding: 0.5rem 1rem!important;" data-dismiss="modal">CANCEL</button>
            </div>
            <div class="col-sm-12 text-center">
                <p id="checkotperror" class="text-danger">You Have Entered Wrong OTP !</p>
            </div>
        </div>
        <div id="DeleteSuccessfully" class="modal-content hidden" style="text-align: center; border-radius: 5px !important;">
            <div class="modal-body" style="padding: 30px!important;">
                <h6 id="bendeletemsg"></h6>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-danger" style="padding: 0.5rem 1rem!important;" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade bd-example-modal-lg" id="ModifyKYCDetail" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="text-align: center; border-radius: 5px !important;">
            <div class="modal-body" style="padding-left: 20px!important;">
                <div class="container" id="Modify1SectionForm">
                    <h3 class="font-weight-400 text-center">Modify KYC Detail
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                    </h3>
                    <div class="bg-light shadow-md rounded p-4 mx-2">
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <asp:TextBox ID="txtKYCFirstName" runat="server" CssClass="form-control" placeholder="First Name"></asp:TextBox>
                            </div>
                            <div class="col-sm-6 form-group">
                                <asp:TextBox ID="txtKYCLastName" runat="server" CssClass="form-control" placeholder="Last Name"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <asp:DropDownList ID="ddlKYCGender" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0">Gender</asp:ListItem>
                                    <asp:ListItem Value="male">Male</asp:ListItem>
                                    <asp:ListItem Value="female">Female</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-sm-6 form-group">
                                <asp:TextBox ID="txtKYCDOB" runat="server" CssClass="form-control" placeholder="Date of Birth [ DD/MM/YYYY ]"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <asp:TextBox ID="txtKYCAddress" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" placeholder="Address as per ID proof"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <asp:DropDownList CssClass="form-control" ID="ddlKYCProof" runat="server">
                                    <asp:ListItem Value="0">Select Proof</asp:ListItem>
                                    <asp:ListItem Value="aadharcard">Aadhar Card</asp:ListItem>
                                    <asp:ListItem Value="votaridcard">Votar Id Card</asp:ListItem>
                                    <asp:ListItem Value="passport">Passport</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-sm-6 form-group">
                                <asp:TextBox ID="txtKYCProofId" runat="server" CssClass="form-control" placeholder="Selected Proof Id number"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-6 form-validation">
                                <label style="font-size: 16px; color: #ff414d; float: left;">Proof Front Image</label>
                                <br />
                                <asp:FileUpload ID="fluKYCFrontImg" runat="server" />
                            </div>
                            <div class="col-sm-6 form-validation">
                                <asp:Image ID="ImgKYCFrontIMage" runat="server" style="width: 100%; border: 1px solid #ccc; border-radius: 5px;" />                                
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-6 form-validation">
                                <label style="font-size: 16px; color: #ff414d; float: left;">Proof Back Image</label>
                                <br />
                                <asp:FileUpload ID="fluKYCBackImg" runat="server" />
                            </div>
                            <div class="col-sm-6 form-validation">
                                <asp:Image ID="ImgKYCBackIMage" runat="server" style="width: 100%; border: 1px solid #ccc; border-radius: 5px;" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <asp:Button ID="btnKYCSubmitKYC" runat="server" class="btn btn-primary btn-block" Style="width: 50%; margin-left: auto; margin-right: auto;" Text="Update KYC Details" OnClick="btnKYCSubmitKYC_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
