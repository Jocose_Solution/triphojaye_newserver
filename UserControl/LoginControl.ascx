﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LoginControl.ascx.vb"
    Inherits="UserControl_LoginControl" %>


<asp:Login ID="UserLogin" runat="server">


    <TextBoxStyle />
    <LoginButtonStyle />
    <LayoutTemplate>


        

        
<%--<section>
		<div class="tr-register">
			<div class="tr-regi-form">
				<h4 style="color:#eee;">Sign In</h4>
				
				<form class="col s12">
					<div class="row">
						<div class="input-field col s12">
							<asp:TextBox runat="server" ID="UserName" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    ErrorMessage="User Name is required." ToolTip="User Id is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>
							<label class="">User Name</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<asp:TextBox ID="Password"  TextMode="Password" runat="server"></asp:TextBox>
                             <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>
							<label class="">Password</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<i class="waves-effect waves-light btn-large full-btn waves-input-wrapper" style="">
                                <asp:Button runat="server" ID="LoginButton" OnClick="LoginButton_Click" Text="Sign In" class="waves-button-input"/>
                                <span class=""></span>

							</i> </div>
					</div>
				</form>
				<p><a href="../ForgotPassword.aspx">forgot password</a> | Are you a new user ? <a href="../regs_new.aspx">Register</a>
				</p>
				
			</div>
		</div>
	</section>--%>


           <div class="theme-hero-area">
      <div class="theme-hero-area-bg-wrap">
        <div class="theme-hero-area-bg" style="background-image:url(../Images/gallery/5BCIl18u-1380x690.jpeg);"></div>
        <div class="theme-hero-area-mask theme-hero-area-mask-strong"></div>
      </div>
      <div class="theme-hero-area-body">
        <div class="theme-page-section _pt-100 theme-page-section-xl">
          <div class="container">
            <div class="row">'


                <div class="col-md-4 col-md-offset-4">

                        <div class="theme-login-header">
                    <h1 class="theme-login-title"></h1>
                    <p class="theme-login-subtitle"></p>
                  </div>

                     
                </div>

              <div class="col-md-4 col-md-offset-4" style="float:right;">
                <div class="theme-login theme-login-white">
                <div class="theme-login-header">
                    <h1 class="theme-login-title">Sign In</h1>
                    <p class="theme-login-subtitle">Login into your Triphojaye Account</p>
                  </div>
                  <div class="theme-login-box">
                    <div class="theme-login-box-inner">
                      <form class="theme-login-form">
                        <div class="form-group theme-login-form-group">
                          <%--<input class="form-control" type="text" placeholder="Email Address"/>--%>
                            <asp:TextBox runat="server" class="form-control-custom" placeholder="User Id" ID="UserName" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    ErrorMessage="User Name is required." ToolTip="User Id is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group theme-login-form-group">
                          <%--<input class="form-control" type="password" placeholder="Password"/>--%>
                            <asp:TextBox ID="Password" class="form-control-custom" placeholder="Password"  TextMode="Password" runat="server"></asp:TextBox>
<asp:Label ID="lblerror" Font-Size="10px" runat="server" ForeColor="Red"></asp:Label>
                             <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>
                          <p class="help-block">
                            <a href="ForgotPassword.aspx" style="color:#fff !important">Forgot password?</a>
                          </p>
                        </div>
                        <div class="form-group">
                          <div class="theme-login-checkbox">
                            <label class="icheck-label">
                              <input class="icheck" type="checkbox" style="border:1px solid #fff !important"/>
                              <span class="icheck-title" style="color: #fff !important;">Keep me logged in</span>
                            </label>
                          </div>
                        </div>
                       <%-- <a class="btn btn-uc btn-dark btn-block btn-lg" href="#">Sign In</a>--%>
                          <asp:Button runat="server" ID="LoginButton" OnClick="LoginButton_Click" Text="Sign In" class="btn btn-uc btn-dark btn-block btn-lg-custom"/>
                      </form>
                     
                      
                    </div>
                    <div class="theme-login-box-alt">
                      <p>Don't have an account? &nbsp;
                        <a href="regs_new.aspx">Sign up.</a>
                      </p>
                    </div>
                  </div>
                  <p class="theme-login-terms">By logging in you accept our
                    <a href="#">terms of use</a>
                    <br/>and
                    <a href="#">privacy policy</a>.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>






        <div class="large-12 medium-12 small-12" style="display:none;">
            <div class="col-md-12  userway "><i class="fa fa-user-circle" aria-hidden="true"></i></div>
            <div class="lft f16" style="display: none;">

                Login Here As
                <asp:DropDownList ID="ddlLogType" runat="server">
                    <asp:ListItem Value="C">Customer</asp:ListItem>
                    <asp:ListItem Value="M">Management</asp:ListItem>
                </asp:DropDownList>
            </div>
            <%--<div class="rgt">
                <a href="ForgotPassword.aspx" rel="lyteframe" class="forgot">Forgot Your password (?)</a>
                </div>--%>
            <div class="clear1">
            </div>

            <div class="form-group has-success has-feedback">

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                    <%--<asp:TextBox ID="UserName" class="form-controlsl " BackColor="White" placeholder="Enter Your User Id" runat="server"></asp:TextBox>--%>
                </div>
                <%--<asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    ErrorMessage="User Name is required." ToolTip="User Id is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>--%>
            </div>
            <div class="form-group has-success has-feedback">

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true" style="    font-size: 23px;" ></i></span>
                   <%-- <asp:TextBox ID="Password" class="form-controlsl" BackColor="White" placeholder="Password" runat="server" TextMode="Password"></asp:TextBox>--%>
                </div>
               <%-- <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>--%>
            </div>



            <div class="large-4 medium-4 small-12 columns">
                <div class="clear1">
                </div>
                <%--<asp:Button ID="LoginButton" runat="server" ValidationGroup="UserLogin" OnClick="LoginButton_Click" CssClass="btnsss"
                    Text="LOGIN" />--%>
                <br />

                <a href="../ForgotPassword.aspx" style="color:#ff0000">Forgot Password</a>
            </div>
            <div class="clear">
            </div>
            <div>
                
            </div>
            <div class="clear">
            </div>
        </div>
    </LayoutTemplate>
    <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
    <TitleTextStyle />     
</asp:Login>



<script>
    var overlay = document.getElementById("overlay");

    // Buttons to 'switch' the page
    var openSignUpButton = document.getElementById("slide-left-button");
    var openSignInButton = document.getElementById("slide-right-button");

    // The sidebars
    var leftText = document.getElementById("sign-in");
    var rightText = document.getElementById("sign-up");

    // The forms
    var accountForm = document.getElementById("sign-in-info")
    var signinForm = document.getElementById("sign-up-info");

    // Open the Sign Up page
    openSignUp = () =>{
        // Remove classes so that animations can restart on the next 'switch'
        leftText.classList.remove("overlay-text-left-animation-out");
    overlay.classList.remove("open-sign-in");
    rightText.classList.remove("overlay-text-right-animation");
    // Add classes for animations
    accountForm.className += " form-left-slide-out"
    rightText.className += " overlay-text-right-animation-out";
    overlay.className += " open-sign-up";
    leftText.className += " overlay-text-left-animation";
    // hide the sign up form once it is out of view
    setTimeout(function(){
        accountForm.classList.remove("form-left-slide-in");
        accountForm.style.display = "none";
        accountForm.classList.remove("form-left-slide-out");
    }, 700);
    // display the sign in form once the overlay begins moving right
    setTimeout(function(){
        signinForm.style.display = "flex";
        signinForm.classList += " form-right-slide-in";
    }, 200);
    }

    // Open the Sign In page
    openSignIn = () =>{
        // Remove classes so that animations can restart on the next 'switch'
        leftText.classList.remove("overlay-text-left-animation");
    overlay.classList.remove("open-sign-up");
    rightText.classList.remove("overlay-text-right-animation-out");
    // Add classes for animations
    signinForm.classList += " form-right-slide-out";
    leftText.className += " overlay-text-left-animation-out";
    overlay.className += " open-sign-in";
    rightText.className += " overlay-text-right-animation";
    // hide the sign in form once it is out of view
    setTimeout(function(){
        signinForm.classList.remove("form-right-slide-in")
        signinForm.style.display = "none";
        signinForm.classList.remove("form-right-slide-out")
    },700);
    // display the sign up form once the overlay begins moving left
    setTimeout(function(){
        accountForm.style.display = "flex";
        accountForm.classList += " form-left-slide-in";
    },200);
    }

    // When a 'switch' button is pressed, switch page
    openSignUpButton.addEventListener("click", openSignUp, false);
    openSignInButton.addEventListener("click", openSignIn, false);
</script>
