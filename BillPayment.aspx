﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="BillPayment.aspx.cs" Inherits="BillPayment" %>

<%@ Register Src="~/UserControl/Recharge/ud_Mobile_Recharge.ascx" TagPrefix="uc1" TagName="ud_Mobile_Recharge" %>
<%@ Register Src="~/UserControl/Recharge/ud_DTH_Recharge.ascx" TagPrefix="uc1" TagName="ud_DTH_Recharge" %>
<%@ Register Src="~/UserControl/Recharge/ud_Electricity_Recharge.ascx" TagPrefix="uc1" TagName="ud_Electricity_Recharge" %>
<%@ Register Src="~/UserControl/Recharge/ud_landline.ascx" TagPrefix="uc1" TagName="ud_landline" %>
<%@ Register Src="~/UserControl/Recharge/ud_Insurance.ascx" TagPrefix="uc1" TagName="ud_Insurance" %>
<%@ Register Src="~/UserControl/Recharge/ud_GAS.ascx" TagPrefix="uc1" TagName="ud_GAS" %>
<%@ Register Src="~/UserControl/Recharge/ud_internet_isp.ascx" TagPrefix="uc1" TagName="ud_internet_isp" %>
<%@ Register Src="~/UserControl/Recharge/ud_water.ascx" TagPrefix="uc1" TagName="ud_water" %>
<%@ Register Src="~/UserControl/Recharge/uc_smbp_transhistory.ascx" TagPrefix="uc1" TagName="uc_smbp_transhistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <link href="icofont/icofont.css" rel="stylesheet" />
    <link href="icofont/icofont.min.css" rel="stylesheet" />

    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <style type="text/css">

        body {
            background: linear-gradient(to right, rgb(66, 39, 90), rgb(115, 75, 109));
        }

        .r-c {
            display: inline-block;
    color: #c2c2c2;
    text-align: center;
    font-size: 15px;
    cursor: pointer;
        }

        .r-c1 {
            margin-top: 4px;
    font-size: 12px;
    color: #080808;
    opacity: .5;
    display: block;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
        }

.panel.with-nav-tabs .panel-heading{
    padding: 5px 5px 0 5px;
}
.panel.with-nav-tabs .nav-tabs{
	border-bottom: none;
}
.panel.with-nav-tabs .nav-justified{
	margin-bottom: -1px;
}
/********************************************************************/
/*** PANEL DEFAULT ***/
.with-nav-tabs.panel-default .nav-tabs > li > a,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: #777;
}
.with-nav-tabs.panel-default .nav-tabs > .open > a,
.with-nav-tabs.panel-default .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-default .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: #777;
	background-color: #ddd;
	border-color: transparent;
}
.with-nav-tabs.panel-default .nav-tabs > li.active > a,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {
	color: #555;
	background-color: #fff;
	border-color: #ddd;
    border-radius: 4px;
    margin-bottom: 6px;
	
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f5f5f5;
    border-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #777;   
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #555;
}
/********************************************************************/
/*** PANEL PRIMARY ***/
.with-nav-tabs.panel-primary .nav-tabs > li > a,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
    color: #fff;
}
.with-nav-tabs.panel-primary .nav-tabs > .open > a,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
	color: #fff;
	background-color: #3071a9;
	border-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
	color: #428bca;
	background-color: #fff;
	border-color: #428bca;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #428bca;
    border-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #fff;   
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    background-color: #4a9fe9;
}
/********************************************************************/
/*** PANEL SUCCESS ***/
.with-nav-tabs.panel-success .nav-tabs > li > a,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
}
.with-nav-tabs.panel-success .nav-tabs > .open > a,
.with-nav-tabs.panel-success .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-success .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
	background-color: #d6e9c6;
	border-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.active > a,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:focus {
	color: #3c763d;
	background-color: #fff;
	border-color: #d6e9c6;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #dff0d8;
    border-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #3c763d;   
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #3c763d;
}
/********************************************************************/
/*** PANEL INFO ***/
.with-nav-tabs.panel-info .nav-tabs > li > a,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
}
.with-nav-tabs.panel-info .nav-tabs > .open > a,
.with-nav-tabs.panel-info .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-info .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
	background-color: #bce8f1;
	border-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.active > a,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:focus {
	color: #31708f;
	background-color: #fff;
	border-color: #bce8f1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #d9edf7;
    border-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #31708f;   
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #31708f;
}
/********************************************************************/
/*** PANEL WARNING ***/
.with-nav-tabs.panel-warning .nav-tabs > li > a,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
}
.with-nav-tabs.panel-warning .nav-tabs > .open > a,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
	background-color: #faebcc;
	border-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:focus {
	color: #8a6d3b;
	background-color: #fff;
	border-color: #faebcc;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #fcf8e3;
    border-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #8a6d3b; 
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #8a6d3b;
}
/********************************************************************/
/*** PANEL DANGER ***/
.with-nav-tabs.panel-danger .nav-tabs > li > a,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
}
.with-nav-tabs.panel-danger .nav-tabs > .open > a,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
	background-color: #ebccd1;
	border-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:focus {
	color: #a94442;
	background-color: #fff;
	border-color: #ebccd1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f2dede; /* bg color */
    border-color: #ebccd1; /* border color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #a94442; /* normal text color */  
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ebccd1; /* hover bg color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff; /* active text color */
    background-color: #a94442; /* active bg color */
}
    </style>
 
    <br />
 
    <div class="col-md-12">
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a class="r-c" href="#tab1default" data-toggle="tab"><i class="icofont-smart-phone icofont-2x"></i><span class="r-c1">Prepaid/Postpaid</span></a></li>
                            <li><a class="r-c" href="#tab2default" data-toggle="tab"><i class="icofont-satellite icofont-2x"></i><span class="r-c1">DTH</span></a></li>
                            <li><a class="r-c" href="#tab3default" data-toggle="tab"><i class="icofont-light-bulb icofont-2x"></i><span class="r-c1">Electricity</span></a></li>
                            <li><a class="r-c" href="#tab4default" data-toggle="tab"><i class="icofont-telephone icofont-2x"></i><span class="r-c1">Landline</span></a></li>
                            <li><a class="r-c" href="#tab5default" data-toggle="tab"><i class="icofont-heart-beat-alt icofont-2x"></i><span class="r-c1">Insurance</span></a></li>
                            <li><a class="r-c" href="#tab6default" data-toggle="tab"><i class="icofont-fire-burn icofont-2x"></i><span class="r-c1">Gas</span></a></li>
                            <li><a class="r-c" href="#tab7default" data-toggle="tab"><i class="icofont-ui-wifi icofont-2x"></i><span class="r-c1">Internet</span></a></li>
                            <li><a class="r-c" href="#tab8default" data-toggle="tab"><i class="icofont-water-drop icofont-2x"></i><span class="r-c1">Water</span></a></li>
                            <li><a class="r-c" href="#tab9default" data-toggle="tab"></a></li>
                               
                        
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">
                            <uc1:ud_Mobile_Recharge runat="server" ID="ud_Mobile_Recharge" />
                        </div>
                        <div class="tab-pane fade" id="tab2default">
                            <uc1:ud_DTH_Recharge runat="server" ID="ud_DTH_Recharge" />
                        </div>
                        <div class="tab-pane fade" id="tab3default">
                            <uc1:ud_Electricity_Recharge runat="server" ID="ud_Electricity_Recharge" />
                        </div>
                        <div class="tab-pane fade" id="tab4default">
                            <uc1:ud_landline runat="server" ID="ud_landline" />
                        </div>
                        <div class="tab-pane fade" id="tab5default">
                            <uc1:ud_Insurance runat="server" ID="ud_Insurance" />
                        </div>
                        <div class="tab-pane fade" id="tab6default">
                            <uc1:ud_GAS runat="server" ID="ud_GAS" />
                        </div>
                        <div class="tab-pane fade" id="tab7default">
                             <uc1:ud_internet_isp runat="server" ID="ud_internet_isp" />
                        </div>
                         <div class="tab-pane fade" id="tab8default">
                             <uc1:ud_water runat="server" ID="ud_water" />
                         </div>
                         <div class="tab-pane fade" id="tab9default">
                             <uc1:uc_smbp_transhistory runat="server" ID="uc_smbp_transhistory" />
                         </div>
                    </div>
                </div>
            </div>
        </div>


    <button type="button" class="btn btn-info btn-lg successmessage hidden" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#SuccessMsg"></button>
    <div class="modal fade" id="SuccessMsg" role="dialog">
        <div class="modal-dialog modal-md" style="margin: 15% auto!important;">
            <div class="modal-content" style="text-align: center; border-radius: 5px !important;">
                <div class="modal-header" style="padding: 12px;">
                    <h5 class="modal-title modelheading"></h5>
                </div>
                <div class="modal-body" style="padding-left: 20px!important;">
                    <h6 class="sucessmsg"></h6>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" style="padding: 0.5rem 1rem!important;" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>






    <div class="container" style="display:none;">


        

        <div class="col-xl-6">


            <section class="mx-2 pb-3">
      
              <ul class="nav with-nav-tabs md-tabs" id="myTabMD" role="tablist">
                <li class="nav-item waves-effect waves-light">
                  <a class="nav-link active" id="mobile-tab-md" data-toggle="tab" href="#mobileTabPills" role="tab" aria-controls="mobileTabPills" aria-selected="true"><i class="icofont-smart-phone icofont-2x"></i></a><span class="">Prepaid/Postpaid</span>
                </li>
                <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="dth-tab-md" data-toggle="tab" href="#dthTabPills" role="tab" aria-controls="dthTabPills" aria-selected="false"><i class="icofont-satellite icofont-2x"></i></a><span class="">DTH</span>
                </li>
                <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="electric-tab-md" data-toggle="tab" href="#thirdTabPills" role="tab" aria-controls="thirdTabPills" aria-selected="false"><i class="icofont-light-bulb icofont-2x"></i></a>
                </li>

                       <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="landline-tab-md" data-toggle="tab" href="#fourthTabPills" role="tab" aria-controls="fourthTabPills" aria-selected="false"><i class="icofont-telephone icofont-2x"></i></a>
                </li>

                       <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="insurance-tab-md" data-toggle="tab" href="#fifthTabPills" role="tab" aria-controls="fifthTabPills" aria-selected="false"><i class="icofont-heart-beat-alt icofont-2x"></i></a>
                </li>

                       <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="gas-tab-md" data-toggle="tab" href="#gasTabPills" role="tab" aria-controls="gasTabPills" aria-selected="false"><i class="icofont-fire-burn icofont-2x"></i></a>
                </li>

                       <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="internet-tab-md" data-toggle="tab" href="#internetTabPills" role="tab" aria-controls="internetTabPills" aria-selected="false"><i class="icofont-ui-wifi icofont-2x"></i></a>
                </li>

                       <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="water-tab-md" data-toggle="tab" href="#waterTabPills" role="tab" aria-controls="waterTabPills" aria-selected="false"><i class="icofont-water-drop icofont-2x"></i></a>
                </li>

                  
              </ul>


              <div class="tab-content card pt-5" id="myTabContentMD">  <%--id="pillsmyTabContent"--%>
               
               
                    <div class="tab-pane  active" id="mobileTabPills" role="tabpanel" aria-labelledby="mobile-tab-md">
                
            </div>

               
                
                   <div class="tab-pane fade" id="dthTabPills" role="tabpanel" aria-labelledby="dth-tab-md">
                
            </div>
             
          
                 
            <div class="tab-pane fade" id="thirdTabPills" role="tabpanel" aria-labelledby="electric-tab-md">
                
            </div>
            

                   
                          <div class="tab-pane fade" id="fourthTabPills" role="tabpanel" aria-labelledby="landline-tab-md">
                
            </div>


                
                       <div class="tab-pane fade" id="fifthTabPills" role="tabpanel" aria-labelledby="insurance-tab-md">
                
            </div>
                      

                 
                       <div class="tab-pane fade" id="gasTabPills" role="tabpanel" aria-labelledby="gas-tab-md">
                
            </div>
                      

                
                      <div class="tab-pane fade" id="internetTabPills" role="tabpanel" aria-labelledby="internet-tab-md">
               
            </div>
                      

                 
                       <div class="tab-pane fade" id="waterTabPills" role="tabpanel" aria-labelledby="water-tab-md">
                
            </div>
                    


                  <div class="tab-pane fade" id="contact-md" role="tabpanel" aria-labelledby="contact-tab-md">
                       <div class="tab-pane fade" id="transHistTabPills" role="tabpanel" aria-labelledby="transHistTab-Pills">
                
            </div>
                        </div>

              </div>
      
            </section>


        </div>


    </div>


    <script type="text/javascript" src="Advance_CSS/billpayment.js"></script>
    <script type="text/javascript" src="Advance_CSS/common.js"></script>

    <script>
        $(document.body).on('click', ".commonDate", function (e) {
            $(this).datepicker({
                dateFormat: "dd/mm/yy",
                showStatus: true,
                showWeeks: true,
                currentText: 'Now',
                autoSize: true,
                maxDate: -0,
                gotoCurrent: true,
                showAnim: 'blind',
                highlightWeek: true
            });
        });

        $('.commonDate').datepicker({
            dateFormat: "dd/mm/yy",
            showStatus: true,
            showWeeks: true,
            currentText: 'Now',
            autoSize: true,
            maxDate: -0,
            gotoCurrent: true,
            showAnim: 'blind',
            highlightWeek: true
        });
    </script>
</asp:Content>

