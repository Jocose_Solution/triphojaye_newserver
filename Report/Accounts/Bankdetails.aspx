﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="true" CodeFile="Bankdetails.aspx.cs" Inherits="SprReports_Accounts_Bankdetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <br />
    <br />
    <section class="">

        
        
		<div class="container">
         
			<div class="row">
				<div class="col-md-5 col-sm-5">
                   
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/SBILogo_state-bank-of-india-new.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">STATE BANK OF INDIA</h4>
						
                    <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	Triphojaye<br />BANK NAME        :	STATE BANK OF INDIA<br />CITY :	Ahmedabad<br />BRANCH           :	BHADRA BRANCH<br />ACCOUNT NO       :	33822414764<br />IFSC code        :	SBIN0060129</p>
        
                          
						</div>
					</div>
                      
				</div>

                <div class="col-md-5 col-sm-5">
                                   
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i aria-hidden="true"><img src="../../Images/Bank/com.kotakprime.jpg" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">KOTAK BANK LTD</h4>
                            <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	Triphojaye<br />BANK NAME        :	KOTAK BANK LTD<br />CITY :	Ahmedabad<br />BRANCH           :		ASHRAM ROAD<br />ACCOUNT NO       :	5711439583<br />IFSC code        :		KKBK0002562</p>
						</div>
					</div>
                     
				</div>

                	<div class="col-md-5 col-sm-5">
                    
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/ICICI-Bank-PNG-Icon-715x715.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">ICICI Bank</h4>
							<p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	Triphojaye<br />BANK NAME        :	ICICI BANK LTD 1<br />CITY :	Ahmedabad<br />BRANCH           :		Khoraj<br />ACCOUNT NO       :	271105000050<br />IFSC code        :			ICIC0002711</p>
						</div>
					</div>
                       
				</div>

				<div class="col-md-5 col-sm-5">                                   
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/Hdfc-Logo.png"" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">HDFC Bank</h4>
							<p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	Triphojaye<br />BANK NAME        :	HDFC BANK<br />CITY :	Ahmedabad<br />BRANCH           :		Mithakhali Six Road<br />ACCOUNT NO       :	00062000025631<br />IFSC code        :			HDFC0000006</p>
                           
                            
						</div>
					</div>
                      
				</div>
				<div class="col-md-5 col-sm-5">
                                            
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/Axis-Bank-Logo.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">Axis Bank</h4>
                            <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	Triphojaye<br />BANK NAME        :	AXIS BANK<br />CITY :	Ahmedabad<br />BRANCH           :		LAW GARDEN BRANCH<br />ACCOUNT NO       :	003010200028680<br />IFSC code        :			UTIB0000003</p>
						</div>
					</div>
                     
				</div>
			
				
                <div class="col-md-5 col-sm-5">
                                           
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i aria-hidden="true"><img src="../../Images/Bank/Boi-Logo-1.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">Bank Of India</h4>
                             <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	Triphojaye<br />BANK NAME        :	BANK OF INDIA<br />CITY :	Ahmedabad<br />BRANCH           :		NAVRANGPURA<br />ACCOUNT NO       :		200920100002992<br />IFSC code        :			BKID0002009</p>
						</div>
					</div>
                     
				</div>
				
			</div>
		</div>

      
     <br />
     <br />
     <br />
        

	</section>



    

</asp:Content>

