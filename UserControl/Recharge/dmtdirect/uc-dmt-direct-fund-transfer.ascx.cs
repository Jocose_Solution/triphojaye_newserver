﻿using InstantPayServiceLib;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DMT_Manager_User_Control_dmtdirect_uc_dmt_direct_fund_transfer : System.Web.UI.UserControl
{
    private static string RegId = string.Empty;
    private static string UserId { get; set; }
    public static string Mobile { get; set; }
    private static string RemitterId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            RegId = string.Empty;

            if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
            {
                string UserId = Session["UID"].ToString();

                string Mobile = Request.QueryString["mobile"] != null ? Request.QueryString["mobile"].ToString() : string.Empty;
                string RemitterId = Request.QueryString["sender"] != null ? Request.QueryString["sender"].ToString() : string.Empty;

                BindPayOutRemitterWithBenDetail(UserId, Mobile, RemitterId);
            }
        }
    }

    protected void btnKYCSubmitKYC_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["RegId"] != null)
            {
                RegId = Session["RegId"].ToString();
                RemitterId = Session["PayRemitterId"].ToString();

                string fn = txtKYCFirstName.Text;
                string ln = txtKYCLastName.Text;
                string add = txtKYCAddress.Text;
                string gen = ddlKYCGender.SelectedValue;
                string dob = txtKYCDOB.Text;
                string proof = ddlKYCProof.SelectedValue;
                string pno = txtKYCProofId.Text;

                string folderPath = Server.MapPath("PayoutKYCImage/" + RemitterId + "/");
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }

                string frontimg = "/dmt-manager/PayoutKYCImage/" + RemitterId + "/frontimg.jpg";
                if (fluKYCFrontImg.HasFile)
                {
                    if (System.IO.File.Exists(frontimg))
                    {
                        System.IO.File.Delete(frontimg);
                    }
                    fluKYCFrontImg.SaveAs(folderPath + "frontimg.jpg");
                }

                string backimg = "/dmt-manager/PayoutKYCImage/" + RemitterId + "/backimg.jpg";
                if (fluKYCBackImg.HasFile)
                {
                    if (System.IO.File.Exists(backimg))
                    {
                        System.IO.File.Delete(backimg);
                    }
                    fluKYCBackImg.SaveAs(folderPath + "backimg.jpg");
                }

                if (InstantPay_PayoutService.UpdateRemitterKYCDetails(RegId, UserId, Mobile, RemitterId, fn, ln, add, gen, dob, proof, pno, frontimg, backimg))
                {
                    //Page_Load(sender, e);
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alert", "alert('KYC details has been updated successfully, Please wait for approval.');window.location.href='/dmt-manager/dmt-direct-fund-transfer.aspx?mobile=" + Session["PayMobile"].ToString() + "&sender=" + Session["PayRemitterId"].ToString() + "'", true);
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    private void BindPayOutRemitterWithBenDetail(string agentid, string mobile, string remitterid)
    {
        try
        {
            if (!string.IsNullOrEmpty(agentid))
            {
                if (!string.IsNullOrEmpty(mobile) && !string.IsNullOrEmpty(remitterid))
                {
                    DataTable BenDetail = new DataTable();

                    DataTable dtPayoutRemitter = InstantPay_PayoutService.GetPayoutRemitterDetails(RegId, agentid, mobile, remitterid, ref BenDetail);

                    if (dtPayoutRemitter != null && dtPayoutRemitter.Rows.Count > 0)
                    {
                        txtKYCFirstName.Text = dtPayoutRemitter.Rows[0]["FirstName"].ToString();
                        txtKYCLastName.Text = dtPayoutRemitter.Rows[0]["LastName"].ToString();
                        ddlKYCGender.Items.FindByValue(dtPayoutRemitter.Rows[0]["Gender"].ToString()).Selected = true;
                        txtKYCDOB.Text = dtPayoutRemitter.Rows[0]["DOB"].ToString();
                        string address = dtPayoutRemitter.Rows[0]["Address"].ToString();
                        txtKYCAddress.Text = address;
                        string idProof = dtPayoutRemitter.Rows[0]["IDProof"].ToString();
                        ddlKYCProof.Items.FindByValue(idProof).Selected = true;
                        txtKYCProofId.Text = dtPayoutRemitter.Rows[0]["IDProofNumber"].ToString();
                        ImgKYCFrontIMage.ImageUrl = dtPayoutRemitter.Rows[0]["FrontImg"].ToString();
                        ImgKYCBackIMage.ImageUrl = dtPayoutRemitter.Rows[0]["BackImg"].ToString();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }
}