﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="false" CodeFile="contact-us.aspx.vb" Inherits="contact_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
  <section>
		<div class="rows contact-map map-container" style="display:none;">


         <iframe src="https://maps.googleapis.com/maps/" allowfullscreen="" style="width: 100%;"><a href="https://www.maps.ie/coordinates.html">find my coordinates</a></iframe>
		</div>
	</section>
    
    <section>
		<div class="form form-spac rows con-page">
			<div class="container">
				<!-- TITLE & DESCRIPTION -->
				<div class="spe-title col-md-12">
					<h2><span>Contact us</span></h2>
					<div class="title-line">
						<div class="tl-1"></div>
						<div class="tl-2"></div>
						<div class="tl-3"></div>
					</div>
					<p>World's leading tour and travels Booking website. Book flight tickets and enjoy your holidays with distinctive experience</p>
				</div>

		<div class="pg-contact">
                     <%--   <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con1">
                            <h2>Triphojaye</h2>
                            <p>Triphojaye is more than just a website or company. Triphojaye is for belief of agents that every agents has for their travelers, to distinctive experience of their travel and to grow.</p>
                        </div>--%>
                        <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con1"> <img src="img/contact/1.html" alt="">
                            <h4>Address</h4>
                            <p>Mumbai, India</p>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con3"> <img src="img/contact/2.html" alt="">
                            <h4>CONTACT INFO:</h4>
                            <p> <a href="tel://0099999999" class="contact-icon">Mobile: +91 9206-85-9206</a>
                                <br> <a href="mailto:mytestmail@gmail.com" class="contact-icon">Email: info@triphojaye.com</a> </p>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con4"> <img src="img/contact/3.html" alt="">
                            <h4>Website</h4>
                            <p> <a href="#">Website: www.triphojaye.com</a>
                            
                                </p>
                        </div>
                    </div>				
			</div>
		</div>
	</section>
    
      
    

</asp:Content>

